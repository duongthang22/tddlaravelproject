<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    /**
     * @param $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

//    public function index(Request $request)
//    {
//        $orderName = $request->input('id');
//        $orderByName = $request->input('name');
//        $keySearch = $request->input('key');
//
//        $tasks = $this->task->latest('id');
//
//        if($orderName)
//        {
//            $tasks = $this->task->orderBy('id', $orderName);
//        }
//
//        if($orderByName)
//        {
//            $tasks = $this->task->orderBy('name', $orderByName);
//        }
//
//        if($orderName && $orderByName)
//        {
//            $tasks = $this->task->orderBy('id', $orderName);
//        }
//
//        if($keySearch)
//        {
//            $tasks = $this->task->where('name', 'like', "%$keySearch%")
//                    ->orwhere('id', $keySearch);
//            if($orderName)
//            {
//                $tasks->orderBy('id', $orderName);
//            }
//        }
//
//        $tasks = $tasks->paginate(10);
//
//        return view('tasks.index', compact('tasks'));
//    }


    public function index(Request $request)
    {
        $orderName = $request->input('id');
        $orderByName = $request->input('name');
        $keySearch = $request->input('key');
        $tasks = $this->task->latest('id');

        if($orderName)
        {
            $tasks = $this->task->orderBy('id', $orderName);
        }

        if($orderByName)
        {
            $tasks = $this->task->orderBy('name', $orderByName);
        }

        if($orderName && $orderByName)
        {
            $tasks = $this->task->orderBy('id', $orderName);
        }

        $this->applySearch($tasks, $keySearch, $orderName);

        $tasks = $tasks->paginate(10);

        return view('tasks.index', compact('tasks'));
    }

    private function applySearch($tasks, $keySearch, $orderName)
    {
        if ($keySearch) {
            $tasks->where(function ($query) use ($keySearch) {
                $query->where('name', 'like', "%$keySearch%")
                    ->orWhere('id', $keySearch);
            });

            if ($orderName) {
                $tasks->orderBy('id', $orderName);
            }
        }
    }

    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function edit($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.edit', compact('task'));
    }

    public function update(UpdateTaskRequest $request, $id)
    {
        $task = $this->task->findOrFail($id);
        $task->update($request->all());
        return redirect()->route('tasks.index');
    }

    public function destroy($id)
    {
        $task = $this->task->findOrFail($id);
        $task->delete();
        return redirect()->route('tasks.index');
    }

    public function show($id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.show', compact('task'));
    }


}
