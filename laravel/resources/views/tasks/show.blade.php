@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8">
            <h2>Task Information</h2>
                <div class="card">
                    <div class="card-body">
                        <label class="uppercase">Name:</label>
                        <p class="text-success m-lg-2">{{ $task->name }}</p>
                    </div>
                    <div class="card-body">
                        <label>Content: </label>
                        <p class="text-success m-lg-2">{{ $task->content }}</p>

                        <button onclick="history.back()" class="btn btn-primary">Back</button>
                    </div>
                </div>
        </div>
    </div>
@endsection

