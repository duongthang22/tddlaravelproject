@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped">
                <tr>
                    <th colspan="2">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="{{ route('tasks.create') }}" class=" btn btn-success">Create new Task</a>
                            </li>
                        </ul>
                    </th>
                    <th></th>
                    <th></th>
                    <th colspan="3">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Sort by ID
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['id' => 'asc'])}}">ID low to high</a></li>
                                        <li><a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['id' => 'desc'])}}">ID high to low</a></li>
                                        <li><a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['name' => 'asc'])}}">Name: a to z</a></li>
                                        <li><a class="dropdown-item" href="{{ request()->fullUrlWithQuery(['name' => 'desc'])}}">Name: z to a</a></li>

                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </th>
                </tr>
                <tr>
                    <th colspan="6">
                        <form style="display: flex">
                            <input type="text" class="form-control" value="{{ isset($_GET['key']) ? $_GET['key'] : '' }}" name="key" placeholder="Enter the task name or id..." style="width: 100%">
                            <button id="btn-search" class="btn btn-primary">Search</button>
                        </form>
                    </th>
                </tr>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th colspan="3">Action</th>
                </tr>

                @foreach($tasks as $task)
                    <tr>
                        <td>{{ $task->id }}</td>
                        <td>{{ $task->name }}</td>
                        <td>{{ $task->content }}</td>

                        <td>
                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary">See</a>
                        </td>
                        <td>
                            <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-warning">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('tasks.delete', $task->id) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        {{ $tasks->links() }}
    </div>
@endsection
