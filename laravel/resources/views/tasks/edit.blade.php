@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8">
            <h2>Edit Task</h2>
            <form action="{{ route('tasks.update', $task->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="card">
                    <div class="card-header">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $task->name }}" placeholder="Name..." style="width: 100%">
                        @error('name')
                        <span id="name-error" class="text-danger error" style="display: block">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                    <div class="card-body">
                        <label>Content</label>
                        <input type="text" class="form-control" name="content" value="{{ $task->content }}" placeholder="Content..." style="width: 100%">
                    </div>


                    <button class="btn-success btn">Submit</button>
                </div>

            </form>
        </div>
    </div>
@endsection
