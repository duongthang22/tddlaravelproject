@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8">
            <h2>Create Task</h2>
            <form action="{{ route('tasks.store') }}" method="POST">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <input type="text" class="form-control" name="name" placeholder="Name..." width="100%">
                        @error('name')
                            <span id="name-error" class="text-danger error" style="display: block">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                    <div class="card-body">
                        <input type="text" class="form-control" name="content" placeholder="Content..." width="100%">
                    </div>

                    <button class="btn-success btn">Submit</button>

                </div>

            </form>
        </div>
    </div>
@endsection
