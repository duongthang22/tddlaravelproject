<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/tasks', [\App\Http\Controllers\TaskController::class, 'index'])
        ->name('tasks.index');

    Route::post('/tasks', [\App\Http\Controllers\TaskController::class, 'store'])
        ->name('tasks.store');

    Route::get('/tasks/create', [\App\Http\Controllers\TaskController::class, 'create'])
        ->name('tasks.create');

    Route::get('/tasks/edit/{id}', [\App\Http\Controllers\TaskController::class, 'edit'])
        ->name('tasks.edit');

    Route::put('/tasks/update/{id}', [\App\Http\Controllers\TaskController::class, 'update'])
        ->name('tasks.update');

    Route::delete('/tasks/delete/{id}', [\App\Http\Controllers\TaskController::class, 'destroy'])
        ->name('tasks.delete');

    Route::get('/tasks/show/{id}', [\App\Http\Controllers\TaskController::class, 'show'])
        ->name('tasks.show');

});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
