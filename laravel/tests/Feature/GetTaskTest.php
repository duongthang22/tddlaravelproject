<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();

        $response = $this->get($this->getShowTaskRoute($task));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
        $response->assertSee($task->name);
    }

    /** @test */
    public function authenticated_user_can_not_get_task_if_task_not_exists()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;

        $response = $this->get('/tasks/show/' . $taskId);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_task()
    {
        $task = Task::factory()->create();

        $response = $this->get($this->getShowTaskRoute($task));
        $response->assertRedirect('/login');
    }

    public function getShowTaskRoute($task)
    {
        return route('tasks.show', ['id' => $task->id]);
    }
}
