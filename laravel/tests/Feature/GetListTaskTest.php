<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();

        $response = $this->get($this->getListTaskRoute());

        $response->assertStatus(Response::HTTP_OK);

        $response->assertViewIs('tasks.index');

        $response->assertSee($task->name);
    }

    /** @test */
    public function authenticated_user_can_see_list_task_search_if_user_import_key_search()
    {
        $this->actingAs(User::factory()->create());

        $task1 = Task::factory()->create(['name' => 'Task1']);
        $task2 = Task::factory()->create(['name' => 'Task2']);

        $response = $this->get($this->getListTaskRoute() . '?key=Task');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task1->name);
        $response->assertSee($task2->name);
    }

    /** @test */
    public function authenticated_user_can_see_list_task_if_user_dont_import_key_search()
    {
        $this->actingAs(User::factory()->create());

        $task1 = Task::factory()->create(['name' => 'Task1']);
        $task2 = Task::factory()->create(['name' => 'Task2']);

        $response = $this->get($this->getListTaskRoute() . '?key=');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task1->name);
        $response->assertSee($task2->name);
    }

    /** @test */
    public function authenticated_user_can_not_see_list_task_if_user_import_key_search_not_exists()
    {
        $this->actingAs(User::factory()->create());

        $task1 = Task::factory()->create(['name' => 'Task1']);
        $task2 = Task::factory()->create(['name' => 'Task2']);

        $keySearchNotExists = 'NonExistKeyWord';

        $response = $this->get($this->getListTaskRoute() . '?key=' . $keySearchNotExists);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertDontSee($task1->name);
        $response->assertDontSee($task2->name);

    }

    /** @test */
    public function authenticated_user_can_sort_if_task_list_exists()
    {
        $this->actingAs(User::factory()->create());

        $task1 = Task::factory()->create(['name' => 'Task1']);
        $task2 = Task::factory()->create(['name' => 'Task2']);

        $response = $this->get($this->getListTaskRoute() . '?sort=asc');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSeeInOrder([$task1->name, $task2->name]);
    }

    /** @test */
    public function authenticated_user_can_not_sort_if_task_list_not_exists()
    {
        $this->actingAs(User::factory()->create());

        $noneExistTaskList = 'NoneExistsTaskList';
        $response = $this->get($this->getListTaskRoute() . '?sort=asc');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertDontSee($noneExistTaskList);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_task()
    {

        $response = $this->get($this->getListTaskRoute());

        $response->assertRedirect('/login');
    }

    public function getListTaskRoute()
    {
        return route('tasks.index');
    }
}
