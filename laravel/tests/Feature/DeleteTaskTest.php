<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteRoute($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
        ]);
    }

    /** @test */
    public function authenticated_user_can_not_delete_if_task_not_exits()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;

        $response = $this->delete(route('tasks.delete', $taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_task_list_if_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());

        $task = Task::factory()->create();
        $response = $this->from($this->getDeleteRoute($task))->get(route('tasks.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteRoute($task));
        $response->assertRedirect('/login');
    }


    public function getDeleteRoute($task)
    {
        return route('tasks.delete', ['id' => $task->id]);
    }
}
