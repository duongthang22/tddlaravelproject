<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_view_edit_task_form()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task));
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function authenticated_user_can_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->put($this->getUpdateTaskRoute($task), $task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', [
            'name' => $task->name,
            'content' => $task->content,
        ]);
    }

    /** @test */
    public function authenticated_user_can_not_edit_if_name_field_is_not_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create(['name' => null]);

        $response = $this->put($this->getUpdateTaskRoute($task), $task->toArray());
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_can_see_name_required_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create(['name' => null]);

        $response = $this->from($this->getEditTaskViewRoute($task))->put($this->getUpdateTaskRoute($task), $task->toArray());
        $response->assertRedirect($this->getEditTaskViewRoute($task));
    }

    /** @test */
    public function authenticated_user_can_not_see_edit_form_if_task_not_exists()
    {
        $this->actingAs(User::factory()->create());
        $taskId = -1;
        $response = $this->get(route('tasks.edit', $taskId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticated_user_can_see_list_task_if_user_can_edit_task()
    {
        $this->actingAs(User::factory()->create());

        $task = Task::factory()->create();

        $response = $this->from($this->getUpdateTaskRoute($task))->get(route('tasks.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
    }


    /** @test */
    public function unauthenticated_user_can_not_see_edit_task_form_view()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticated_user_can_not_edit_task()
    {
        $task = Task::factory()->create();
        $response = $this->put($this->getUpdateTaskRoute($task), $task->toArray());
        $response->assertRedirect('/login');
    }

    public function getEditTaskViewRoute($task)
    {
        return route('tasks.edit', ['id' => $task->id]);
    }

    public function getUpdateTaskRoute($task)
    {
        return route('tasks.update', ['id' => $task->id]);
    }
}
