<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /** @test */
    public function user_gets_404_if_user_import_wrong_url()
    {
        $wrongUrl = 'pageNotFound';
        $response = $this->get($wrongUrl);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
